use std::{
    collections::{HashMap, VecDeque},
    f32::consts::{FRAC_1_SQRT_2, SQRT_2},
    hash::{Hash, Hasher},
};

use bevy::{
    input::{mouse::MouseButtonInput, ButtonState},
    math::I16Vec2,
    prelude::*,
    window::PrimaryWindow,
};
use priority_queue::PriorityQueue;

use crate::{
    data::{
        map_position::{self, DontSync, MapPosition},
        MapData, TileData,
    },
    pawn::{Destination, Pawn},
    FIXED_FRAME_TIME,
};

use super::{get_cursor_position, MainCamera};

#[derive(Resource, Default, Debug)]
pub struct Selection {
    pawns: Vec<Entity>,
}

pub fn select_pawns(
    mut selection_start_position: Local<Option<Vec2>>,
    mut selection: ResMut<Selection>,
    mut mouse_buttons: EventReader<MouseButtonInput>,
    mut gizmos: Gizmos,
    window: Query<&Window, With<PrimaryWindow>>,
    camera: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
    pawns: Query<(Entity, &GlobalTransform), With<Pawn>>,
) {
    for event in mouse_buttons.read() {
        if event.button == MouseButton::Left {
            let camera = camera.single();
            let window = window.single();

            let Some(event_world_position) = get_cursor_position(camera.0, camera.1, window) else {
                continue;
            };

            match event.state {
                ButtonState::Pressed => {
                    *selection_start_position = Some(event_world_position.to_owned());
                }
                ButtonState::Released => {
                    let Some(selection_start) = *selection_start_position else {
                        continue;
                    };
                    *selection_start_position = None;
                    let selection_end = event_world_position.to_owned();

                    selection.pawns = pawns
                        .iter()
                        .filter(|pawn| {
                            Rect::from_corners(selection_start, selection_end)
                                .contains(pawn.1.translation().xy())
                        })
                        .map(|pawn| pawn.0)
                        .collect();
                }
            }
        }
    }

    // Draw selection gizmo
    if let Some(selection_start) = *selection_start_position {
        let camera = camera.single();
        let window = window.single();

        let Some(current_position) = get_cursor_position(camera.0, camera.1, window) else {
            return;
        };

        gizmos.rect_2d(
            selection_start.lerp(current_position, 0.5),
            0.,
            (selection_start - current_position).abs(),
            Color::WHITE,
        );
    }
}

pub fn draw_gizmos_for_selected_pawns(
    mut gizmos: Gizmos,
    selection: Res<Selection>,
    pawns: Query<(Entity, &GlobalTransform), With<Pawn>>,
) {
    pawns
        .iter()
        .filter(|pawn| selection.pawns.contains(&pawn.0))
        .for_each(|pawn| {
            gizmos.circle_2d(pawn.1.translation().xy(), 0.5, Color::WHITE);
        });
}

pub fn draw_grid_under_cursor(
    mut gizmos: Gizmos,
    window: Query<&Window, With<PrimaryWindow>>,
    camera: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
) {
    let camera = camera.single();
    let window = window.single();

    let Some(current_position) = get_cursor_position(camera.0, camera.1, window) else {
        return;
    };

    gizmos.rect_2d(current_position.round(), 0., Vec2::splat(1.), Color::BLACK);
}

pub fn set_destination_for_selection(
    mut commands: Commands,
    selection: Res<Selection>,
    mut pawns: Query<(Entity, &Transform, &mut MapPosition, Option<&CurrentPath>), With<Pawn>>,
    mouse: Res<ButtonInput<MouseButton>>,
    window: Query<&Window, With<PrimaryWindow>>,
    camera: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
) {
    if mouse.just_pressed(MouseButton::Right) {
        let camera = camera.single();
        let window = window.single();

        let Some(current_position) = get_cursor_position(camera.0, camera.1, window) else {
            return;
        };

        selection.pawns.iter().for_each(|pawn| {
            let mut pawn = pawns
                .get_mut(*pawn)
                .expect("Pawn in selection does not exist");

            commands
                .entity(pawn.0)
                .insert(Destination(current_position.round().as_i16vec2()));

            if let Some(_) = pawn.3 {
                pawn.2 .0 = pawn.1.translation.xy().as_i16vec2();

                commands
                    .entity(pawn.0)
                    .remove::<CurrentPath>()
                    .remove::<DontSync<MapPosition>>();
            }
        });
    }
}

pub fn calculate_path_for_all_pawns_with_destination(
    mut commands: Commands,
    pawns: Query<(Entity, &MapPosition, &Destination), With<Pawn>>,
    map_data: Res<MapData>,
) {
    for pawn in pawns.iter() {
        commands.entity(pawn.0).remove::<Destination>();

        let Some(path) = pathfind(&map_data, **pawn.1, **pawn.2) else {
            return;
        };

        commands
            .entity(pawn.0)
            .insert((CurrentPath(path), DontSync::<MapPosition>::new()));
    }
}

pub fn draw_current_paths(mut gizmos: Gizmos, paths: Query<&CurrentPath>) {
    for path in paths.iter() {
        gizmos.linestrip_2d(
            path.0.nodes.iter().map(|node| node.position.as_vec2()),
            Color::WHITE,
        );
    }
}

/// Fixed update
pub fn move_pawns_on_path(
    mut commands: Commands,
    mut pawns: Query<(
        Entity,
        &Pawn,
        &mut Transform,
        &mut MapPosition,
        &mut CurrentPath,
    )>,
) {
    for (pawn_entity, pawn, mut pawn_transform, mut map_position, mut pawn_path) in pawns.iter_mut()
    {
        let mut remaining_move = pawn.move_speed * FIXED_FRAME_TIME;
        while remaining_move > 0. {
            if let Some(node) = pawn_path.nodes.get(1) {
                let node_position = node.position.as_vec2();
                let diff = node_position - pawn_transform.translation.xy();
                let diff_lenght = diff.length();

                pawn_transform.translation = if diff_lenght <= remaining_move {
                    map_position.0 = node.position;
                    pawn_path.nodes.pop_front();
                    node_position.extend(0.)
                } else {
                    let dirr = (node.position - pawn_path.nodes.get(0).unwrap().position).as_vec2()
                        * if node.is_diagonal_with_prev {
                            FRAC_1_SQRT_2
                        } else {
                            1.
                        };
                    pawn_transform.translation + (dirr * remaining_move).extend(0.)
                };

                remaining_move -= diff_lenght;
            } else {
                commands
                    .entity(pawn_entity)
                    .remove::<CurrentPath>()
                    .remove::<DontSync<MapPosition>>();
                break;
            }
        }
    }
}

#[derive(Debug)]
pub struct PathNode {
    pub is_diagonal_with_prev: bool,
    pub position: I16Vec2,
}

pub struct Path {
    pub nodes: VecDeque<PathNode>,
}

#[derive(Component, Deref, DerefMut)]
#[component(storage = "SparseSet")]
pub struct CurrentPath(pub Path);

#[derive(Clone, Copy, Default, Debug)]
struct AStarNode {
    previous: I16Vec2,
    current: I16Vec2,
    accumulated_cost: f32,
    heuristic_cost: f32,
}

impl AStarNode {
    pub fn cost(&self) -> f32 {
        self.accumulated_cost + self.heuristic_cost
    }
    pub fn cost_i32(&self) -> i32 {
        (self.cost() * 256.) as i32
    }
}
impl Hash for AStarNode {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.current.hash(state);
    }
}

impl Eq for AStarNode {}

impl PartialEq for AStarNode {
    fn eq(&self, other: &Self) -> bool {
        self.current == other.current
    }
}

impl PartialOrd for AStarNode {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        other.cost().partial_cmp(&self.cost())
    }
}

impl Ord for AStarNode {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.cost().total_cmp(&self.cost())
    }
}

// TODO: Fix pawns passing diagonally through diagonal walls
pub fn pathfind(map_data: &MapData, start: I16Vec2, destination: I16Vec2) -> Option<Path> {
    let Some(tile) = map_data.at_world(destination) else {
        return None;
    };
    if tile.move_speed.is_none() {
        return None;
    }

    let mut open: PriorityQueue<AStarNode, i32> = PriorityQueue::new();
    let mut closed = HashMap::new();

    let node = AStarNode {
        previous: start,
        current: start,
        accumulated_cost: 0.,
        heuristic_cost: heuristic_cost(start, destination),
    };

    open.push(node, -node.cost_i32());

    loop {
        let Some((node, _)) = open.pop() else {
            return None;
        };
        closed.insert(node.current, node);

        if node.current == destination {
            return Some(Path {
                nodes: collect_path(closed, destination),
            });
        }

        for (i, tile) in neighbours(&map_data, &node.current).iter().enumerate() {
            let Some((tile_position, tile)) = tile else {
                continue;
            };
            let Some(move_speed) = tile.move_speed else {
                continue;
            };

            if closed.contains_key(&tile_position) {
                continue;
            }

            let is_diagonal = match i {
                0 | 2 | 5 | 7 => true,
                _ => false,
            };

            // TODO: Those calculations can be made faster by switching fixed point arithmetics
            let accumulated_cost =
                node.accumulated_cost + ((1. / move_speed) * if is_diagonal { SQRT_2 } else { 1. });
            let heuristic_cost = heuristic_cost(*tile_position, destination);
            let node = AStarNode {
                previous: node.current,
                current: *tile_position,
                accumulated_cost,
                heuristic_cost,
            };

            let neighbour = open.get_mut(&node);

            if let Some((neighbour, _)) = neighbour {
                if neighbour.accumulated_cost > node.accumulated_cost {
                    *neighbour = node;

                    open.change_priority(&node, -node.cost_i32()).unwrap();
                }
            } else {
                open.push(node, -node.cost_i32());
            };
        }
    }
}

pub fn heuristic_cost(from: I16Vec2, to: I16Vec2) -> f32 {
    // Chebyshev
    let distance_x = (from.x - to.x).abs() as f32;
    let distance_y = (from.y - to.y).abs() as f32;

    if distance_x > distance_y {
        SQRT_2 * distance_y + (distance_x - distance_y)
    } else {
        SQRT_2 * distance_x + (distance_y - distance_x)
    }
}

pub fn neighbours<'a>(
    map_data: &'a MapData,
    node: &'a I16Vec2,
) -> [Option<(I16Vec2, &'a TileData)>; 8] {
    let mut result = [None; 8];
    let mut i = 0;
    for y in (node.y - 1)..=(node.y + 1) {
        for x in (node.x - 1)..=(node.x + 1) {
            if x == node.x && y == node.y {
                continue;
            }

            let Some(tile) = map_data.at_world_xy(&x, &y) else {
                i += 1;
                continue;
            };

            result[i] = Some((I16Vec2::new(x, y), tile));
            i += 1;
        }
    }
    result
}

fn collect_path(mut visited: HashMap<I16Vec2, AStarNode>, end: I16Vec2) -> VecDeque<PathNode> {
    let mut result = VecDeque::new();
    let mut current_position = end;

    loop {
        let node = visited.remove(&current_position).unwrap();
        result.push_front(PathNode {
            is_diagonal_with_prev: node.current.x != node.previous.x
                && node.current.y != node.previous.y,
            position: node.current,
        });

        if node.current == node.previous {
            return result;
        }

        current_position = node.previous;
    }
}
