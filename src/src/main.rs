use assets::*;
use bevy::{
    diagnostic::FrameTimeDiagnosticsPlugin,
    math::U16Vec2,
    prelude::*,
    winit::{UpdateMode::Continuous, WinitSettings},
};
use bevy_asset_loader::prelude::*;
use bevy_editor_pls::prelude::*;
use bevy_fast_tilemap::FastTileMapPlugin;
use game_mode_main::GameModeMainPlugin;
use misc::screenshot::ScreenshotPlugin;
use pawn::animate_sprites;

mod assets;
mod camera;
mod data;
mod game_mode_main;
mod misc;
mod pawn;

const FIXED_FRAME_TIME: f32 = 1. / 64.;

#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash, States, Default)]
enum AssetLoadingState {
    #[default]
    Loading,
    Loaded,
}

#[derive(Clone, Copy, Eq, PartialEq, Debug, Hash, States, Default)]
enum GameMode {
    #[default]
    StartMenu,
    MainStartup,
    MainUpdate,
}

#[derive(Resource, Clone, Copy, Deref, DerefMut)]
struct MapSize(U16Vec2);

const TILE_MAP_OFFSET: Vec2 = Vec2::new(0.5, 0.5);

fn main() {
    App::new()
        .insert_resource(MapSize(U16Vec2::splat(512)))
        .init_state::<GameMode>()
        .add_plugins((DefaultPlugins))
        .add_plugins(FastTileMapPlugin::default())
        .add_plugins((EditorPlugin::default(), FrameTimeDiagnosticsPlugin))
        .add_plugins((
            SetupPlugin, //
            GameModeMainPlugin,
        ))
        .add_plugins((
            // PositionTextPlugin, //
            ScreenshotPlugin,
            // input::InputPlugin,
        ))
        .insert_resource(WinitSettings {
            focused_mode: Continuous,
            unfocused_mode: Continuous,
        })
        .add_systems(Update, animate_sprites)
        .run();
}

struct SetupPlugin;

impl Plugin for SetupPlugin {
    fn build(&self, app: &mut App) {
        app
            // SetupWorld
            .insert_resource(ClearColor(Color::BLACK))
            .insert_resource(AmbientLight {
                color: Color::WHITE,
                brightness: 5.0,
            })
            // SetupAssets
            .add_systems(PreStartup, GenAssets::init)
            .init_state::<AssetLoadingState>()
            .add_loading_state(
                LoadingState::new(AssetLoadingState::Loading)
                    .continue_to_state(AssetLoadingState::Loaded)
                    .load_collection::<TileAssets>()
                    .load_collection::<PawnAssets>()
                    .load_collection::<BasicAssets>(),
            )
            .add_systems(
                OnEnter(AssetLoadingState::Loaded),
                |mut game_mode_state: ResMut<NextState<GameMode>>| {
                    game_mode_state.set(GameMode::MainStartup)
                },
            );
    }
}
