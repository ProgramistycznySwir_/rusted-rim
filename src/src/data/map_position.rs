use std::marker::PhantomData;

use bevy::{math::I16Vec2, prelude::*};

/// Syncs MapPosition -> Transform
#[derive(Component, Deref, DerefMut)]
pub struct MapPosition(pub I16Vec2);

/// To disable syncing position for some entities
#[derive(Component, Default)]
#[component(storage = "SparseSet")]
pub struct DontSync<T>(pub PhantomData<T>);

impl<T> DontSync<T> {
    pub fn new() -> Self {
        Self(PhantomData::<T>)
    }
}

pub fn sync_map_position(
    mut query: Query<
        (&mut Transform, &MapPosition),
        (Changed<MapPosition>, Without<DontSync<MapPosition>>),
    >,
) {
    query
        .par_iter_mut()
        .for_each(|(mut transform, map_position)| {
            transform.translation.x = map_position.x as f32;
            transform.translation.y = map_position.y as f32;
        })
}
