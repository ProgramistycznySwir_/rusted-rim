use bevy::prelude::*;
use bevy_asset_loader::prelude::*;

pub const TILE_ASSETS_BASIC_TILE_SIZE: f32 = 16.;
pub const TILE_ASSETS_CRAFTING_MATERIALS_TILE_SIZE: f32 = 24.;
pub const TILE_ASSETS_GRASS_TILE_SIZE: f32 = 32.;
pub const TILE_ASSETS_WATER_TILE_SIZE: f32 = TILE_ASSETS_GRASS_TILE_SIZE;

#[derive(Resource, AssetCollection)]
pub struct TileAssets {
    #[asset(texture_atlas_layout(tile_size_x = 16., tile_size_y = 16., columns = 32, rows = 32))]
    pub basic_atlas: Handle<TextureAtlasLayout>,
    #[asset(path = "Textures-16.png")]
    pub basic: Handle<Image>,

    #[asset(texture_atlas_layout(tile_size_x = 24., tile_size_y = 24., columns = 11, rows = 11))]
    pub crafting_materials_atlas: Handle<TextureAtlasLayout>,
    #[asset(path = "crafting_materials/resources_basic.png")]
    pub crafting_materials: Handle<Image>,

    #[asset(path = "grass.png")]
    pub grass: Handle<Image>,
    #[asset(path = "water.png")]
    pub water: Handle<Image>,
}

#[derive(Resource, AssetCollection)]
pub struct PawnAssets {
    #[asset(texture_atlas_layout(tile_size_x = 32., tile_size_y = 32., columns = 8, rows = 10))]
    pub human_male_atlas: Handle<TextureAtlasLayout>,
    #[asset(path = "playerSprites_ [version 1.0]/mPlayer_ [human].png")]
    pub human_male: Handle<Image>,
}

#[derive(Resource, AssetCollection)]
pub struct BasicAssets {
    // #[asset(path = "fonts/FiraSans-Bold.ttf")]
    // pub font_firaSans: Handle<Font>
}

/// Assets that are generated for game.
///
/// Don't forget to initialize it by calling `Self::init()`
#[derive(Resource)]
pub struct GenAssets {
    pub cube_mesh: Handle<Mesh>,
    pub sphere_mesh: Handle<Mesh>,
    pub orange_red_material: Handle<StandardMaterial>,
    pub orange_red_material_emissive_3: Handle<StandardMaterial>,
    pub projectile_material: Handle<StandardMaterial>,
    pub white_material: Handle<StandardMaterial>,
    pub white_material_emissive_1: Handle<StandardMaterial>,
    pub white_material_emissive_3: Handle<StandardMaterial>,
    pub white_material_emissive_5: Handle<StandardMaterial>,
    pub white_material_transparent_20: Handle<StandardMaterial>,
}

impl GenAssets {
    /// Startup system
    pub fn init(
        mut commands: Commands,
        _server: Res<AssetServer>,
        mut meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
    ) {
        commands.insert_resource(Self {
            cube_mesh: meshes.add(Mesh::from(Cuboid::new(1., 1., 1.))),
            sphere_mesh: meshes.add(Mesh::from(Sphere::new(0.5))),
            orange_red_material: materials.add(Color::ORANGE_RED),
            orange_red_material_emissive_3: gen_emissive_material(
                &mut materials,
                Color::ORANGE_RED,
                &3.,
            ),
            white_material: materials.add(Color::WHITE),
            projectile_material: gen_emissive_material(&mut materials, Color::RED, &20.),
            white_material_emissive_1: gen_emissive_material(&mut materials, Color::WHITE, &1.),
            white_material_emissive_3: gen_emissive_material(&mut materials, Color::WHITE, &3.),
            white_material_emissive_5: gen_emissive_material(&mut materials, Color::WHITE, &5.),
            white_material_transparent_20: materials.add(StandardMaterial {
                base_color: Color::rgba(1., 1., 1., 0.2),
                alpha_mode: AlphaMode::Blend,
                double_sided: true,
                ..default()
            }),
        });
    }
}

fn gen_emissive_material(
    materials: &mut Assets<StandardMaterial>,
    base_color: Color,
    intensity: &f32,
) -> Handle<StandardMaterial> {
    materials.add(StandardMaterial {
        base_color,
        emissive: Color::rgb_linear(*intensity, *intensity, *intensity).into(),
        unlit: true,
        ..default()
    })
}
