use bevy::{
    input::ButtonInput,
    math::Vec3,
    prelude::*,
    render::camera::{Camera, ScalingMode},
};

#[derive(Component)]
pub struct MainCamera;

pub fn spawn_camera(mut commands: Commands) {
    let mut camera_bundle = Camera2dBundle::default();
    camera_bundle.projection.scaling_mode = ScalingMode::FixedVertical(20.);
    commands.spawn((camera_bundle, MainCamera));
}

// A simple camera system for moving and zooming the camera.
#[allow(dead_code)]
pub fn camera_movement(
    time: Res<Time>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut query: Query<(&mut Transform, &mut OrthographicProjection), With<Camera>>,
) {
    for (mut transform, mut ortho) in query.iter_mut() {
        let mut direction = Vec3::ZERO;

        if keyboard_input.pressed(KeyCode::KeyA) {
            direction -= Vec3::new(1.0, 0.0, 0.0);
        }

        if keyboard_input.pressed(KeyCode::KeyD) {
            direction += Vec3::new(1.0, 0.0, 0.0);
        }

        if keyboard_input.pressed(KeyCode::KeyW) {
            direction += Vec3::new(0.0, 1.0, 0.0);
        }

        if keyboard_input.pressed(KeyCode::KeyS) {
            direction -= Vec3::new(0.0, 1.0, 0.0);
        }

        const ZOOM_RATE: f32 = 1.5;

        if keyboard_input.just_pressed(KeyCode::KeyZ) {
            ortho.scale *= ZOOM_RATE;
        }

        if keyboard_input.just_pressed(KeyCode::KeyX) {
            ortho.scale /= ZOOM_RATE;
        }

        ortho.scale = ortho.scale.clamp(0.5, 20.);

        let z = transform.translation.z;
        transform.translation += time.delta_seconds() * direction * 50. * ortho.scale;
        // Important! We need to restore the Z values when moving the camera around.
        // Bevy has a specific camera setup and this can mess with how our layers are shown.
        transform.translation.z = z;
    }
}

/// ```rs
/// // Could be convinently used like this:
/// let Some(current_position) = get_cursor_position(camera.0, camera.1, window) else {
///     return;
/// };
/// ```
pub fn get_cursor_position(
    camera: &Camera,
    camera_transform: &GlobalTransform,
    window: &Window,
) -> Option<Vec2> {
    Some(camera.viewport_to_world_2d(camera_transform, window.cursor_position()?)?)
}
