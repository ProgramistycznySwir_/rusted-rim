use bevy::{math::I16Vec2, prelude::*};

use crate::data::map_position::MapPosition;

#[derive(Component)]
pub struct Pawn {
    pub move_speed: f32,
}

#[derive(Component, Deref)]
#[component(storage = "SparseSet")]
pub struct Destination(pub I16Vec2);

#[derive(Bundle)]
pub struct PawnBundle {
    pawn: Pawn,
    position: MapPosition,
}

impl PawnBundle {
    pub fn spawn_pawn(
        commands: &mut Commands,
        name: String,
        sprite: Handle<Image>,
        atlas: Handle<TextureAtlasLayout>,
        // text: Handle<TextureAtlas>,
        position: I16Vec2,
    ) -> Entity {
        commands
            .spawn((
                PawnBundle {
                    pawn: Pawn { move_speed: 2. },
                    position: MapPosition(position),
                },
                SpatialBundle::from_transform(Transform::from_translation(
                    position.as_vec2().extend(0.),
                )),
                Name::new(name),
            ))
            .with_children(|parent| {
                parent.spawn((
                    SpriteSheetBundle {
                        texture: sprite,
                        atlas: TextureAtlas {
                            layout: atlas,
                            index: 8,
                        },
                        transform: Transform::from_xyz(0., 0., 3.)
                            .with_scale(Vec3::splat(1. / 32.)),
                        ..default()
                    },
                    AnimationIndices::Range { first: 8, last: 11 },
                    AnimationTimer(Timer::from_seconds(0.2, TimerMode::Repeating)),
                ));
            })
            .id()
    }
}

#[derive(Component)]
pub enum AnimationIndices {
    Range { first: usize, last: usize },
}

#[derive(Component, Deref, DerefMut)]
pub struct AnimationTimer(Timer);

pub fn animate_sprites(
    time: Res<Time>,
    mut query: Query<(&AnimationIndices, &mut AnimationTimer, &mut TextureAtlas)>,
) {
    for (indices, mut timer, mut atlas) in &mut query {
        timer.tick(time.delta());
        if let AnimationIndices::Range { first, last } = indices {
            if timer.just_finished() {
                atlas.index = if atlas.index == *last {
                    *first
                } else {
                    atlas.index + 1
                };
            }
        }
    }
}
