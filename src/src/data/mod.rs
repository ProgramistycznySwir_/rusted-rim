use bevy::{
    ecs::system::Resource,
    prelude::{Deref, DerefMut},
};

pub mod chunk;
pub mod map;
pub mod map_position;

pub use map::*;

#[derive(Clone, Copy, Default, Debug)]
pub struct TileData {
    pub height: f64,
    pub is_water: bool,
    pub is_deep_water: bool,
    pub is_rock: bool,
    /// If None, this tile is impassable
    pub move_speed: Option<f32>,
}
#[derive(Resource, Deref, DerefMut)]
pub struct MapData(pub map::GameMap<TileData>);
