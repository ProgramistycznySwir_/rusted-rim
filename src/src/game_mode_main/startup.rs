use bevy::math::I16Vec2;
use bevy::prelude::*;
use bevy_fast_tilemap::{Map, MapBundleManaged};
use noise::{NoiseFn, Perlin};

use crate::assets::*;
use crate::data::{GameMap, MapData, TileData};
use crate::pawn::PawnBundle;
use crate::{MapSize, TILE_MAP_OFFSET};

/// Marker
#[derive(Component)]
pub struct BaseMap;
/// Marker
#[derive(Component)]
pub struct WaterMap;

pub fn setup_tilemaps(
    mut commands: Commands,
    map_size: Res<MapSize>,
    tiles: Res<TileAssets>,
    mut materials: ResMut<Assets<Map>>,
) {
    let map = Map::builder(
        map_size.0.as_uvec2(),
        tiles.grass.to_owned(),
        Vec2::splat(TILE_ASSETS_GRASS_TILE_SIZE),
    )
    .build();

    let mut map_bundle = MapBundleManaged::new(map, materials.as_mut());
    map_bundle.transform.scale = Vec3::splat(1. / TILE_ASSETS_GRASS_TILE_SIZE);
    map_bundle.transform.translation = TILE_MAP_OFFSET.extend(0.);

    commands.spawn((map_bundle, BaseMap));

    let map = Map::builder(
        map_size.0.as_uvec2(),
        tiles.water.to_owned(),
        Vec2::splat(TILE_ASSETS_WATER_TILE_SIZE),
    )
    .build();

    let mut map_bundle = MapBundleManaged::new(map, materials.as_mut());
    map_bundle.transform.scale = Vec3::splat(1. / TILE_ASSETS_WATER_TILE_SIZE);
    map_bundle.transform.translation = TILE_MAP_OFFSET.extend(1.);

    commands.spawn((map_bundle, WaterMap));
}

pub fn generate_terrain_data(map_size: Res<MapSize>, mut commands: Commands) {
    let mut map = Vec::with_capacity(map_size.x as usize * map_size.y as usize);

    let rng = Perlin::new(1);

    for y in 0..map_size.y {
        for x in 0..map_size.x {
            let height =
                sample_multi_layer_noise(&rng, &(x as f64), &(y as f64), 1., 32., 2, &0.5, &2.0);

            let height = height - 0.1;

            let is_rock = height > 0.5;
            let is_deep_water = height < 0.05;

            map.push(TileData {
                height: height - 0.1,
                is_water: height < 0.15,
                is_deep_water,
                is_rock,
                move_speed: if is_deep_water || is_rock {
                    None
                } else {
                    // Below grass level
                    if height < 0.15 {
                        Some(0.5)
                    } else if height < 0.2 {
                        Some(0.8)
                    } else {
                        Some(1.)
                    }
                },
            });
        }
    }

    commands.insert_resource(MapData(GameMap::new(
        map,
        map_size.0,
        I16Vec2::new(map_size.0.x as i16 / 2 - 1, map_size.0.y as i16 / 2),
    )))
}

pub fn fill_tiles_in_base_map(
    mut map_materials: ResMut<Assets<Map>>,
    map: Query<&Handle<Map>, With<BaseMap>>,
    map_data: Res<MapData>,
) {
    let Some(map) = map_materials.get_mut(map.single()) else {
        warn!("No map material");
        return;
    };

    let mut map = map.indexer_mut();

    for y in 0..map.size().y {
        for x in 0..map.size().x {
            let tile = &map_data.at_xy(x as u16, y as u16);

            let id = if tile.is_rock {
                if tile.height > 0.75 {
                    1
                } else {
                    0
                }
            } else if tile.height > 0.2 {
                let relative_height = (tile.height - 0.2) / (0.5 - 0.2);
                2.0.lerp(4.0, relative_height) as u32
            } else {
                let relative_height = tile.height / 0.2;
                8.0.lerp(5.0, relative_height) as u32
            };

            map.set(x, y, id);
        }
    }
}

pub fn fill_tiles_in_water_map(
    mut map_materials: ResMut<Assets<Map>>,
    map: Query<&Handle<Map>, With<WaterMap>>,
    map_data: Res<MapData>,
) {
    let Some(map) = map_materials.get_mut(map.single()) else {
        warn!("No map material");
        return;
    };

    let mut map = map.indexer_mut();

    for y in 0..map.size().y {
        for x in 0..map.size().x {
            let tile = &map_data.at_xy(x as u16, y as u16);

            let id = if tile.is_water {
                if tile.is_deep_water {
                    2
                } else {
                    1
                }
            } else {
                0
            };

            map.set(x, y, id);
        }
    }
}

/// `periodicity` - 1/frequency
pub fn sample_multi_layer_noise<T: NoiseFn<f64, 2>>(
    rng: &T,
    x: &f64,
    y: &f64,
    mut amplitude: f64,
    mut periodicity: f64,
    octaves: u8,
    lacunarity: &f64,
    persistence: &f64,
) -> f64 {
    let mut sample: f64 = 0.;
    let mut normalization_factor = 0.;

    for _ in 0..octaves {
        sample += amplitude * rng.get([x / periodicity, y / periodicity]);
        normalization_factor += amplitude;

        amplitude *= persistence;
        periodicity /= lacunarity;
    }

    ((sample / normalization_factor) + 1.) / 2.
}

pub fn spawn_pawns(mut commands: Commands, pawn_assets: Res<PawnAssets>, map_data: Res<MapData>) {
    // Find free position
    let mut position = I16Vec2::ZERO;
    loop {
        if map_data.at_world(position).unwrap().move_speed.is_some() {
            break;
        }

        position += I16Vec2::new(0, 1);
    }

    PawnBundle::spawn_pawn(
        &mut commands,
        "Bob".to_owned(),
        pawn_assets.human_male.to_owned(),
        pawn_assets.human_male_atlas.to_owned(),
        position,
    );

    let mut position = I16Vec2::new(5, 5);
    loop {
        if map_data.at_world(position).unwrap().move_speed.is_some() {
            break;
        }

        position += I16Vec2::new(0, 1);
    }

    PawnBundle::spawn_pawn(
        &mut commands,
        "Bob".to_owned(),
        pawn_assets.human_male.to_owned(),
        pawn_assets.human_male_atlas.to_owned(),
        position,
    );
}
