use bevy::{
    math::{I16Vec2, U16Vec2},
    prelude::*,
    transform::commands,
};

use crate::MapSize;

use super::GameMap;

pub struct Chunk {}

const CHUNK_SIZE: u16 = 8;

#[derive(Resource)]
pub struct Chunks {
    world_offset: I16Vec2,
    map_size: U16Vec2,
    map: Vec<Chunk>,
}

impl Chunks {
    // pub fn new(map: Vec<Chunk>, map_size: U16Vec2, world_offset: I16Vec2) -> Self {
    //     Self {
    //         world_offset,
    //         map_size,
    //         map_size_x: map_size.x as usize,
    //         map,
    //     }
    // }

    // pub fn at_xy(&self, x: u16, y: u16) -> &Chunk {
    //     self.map
    //         .get(x as usize + y as usize * self.map_size_x)
    //         .unwrap()
    // }

    // pub fn at(&self, vec: U16Vec2) -> &T {
    //     self.at_xy(vec.x, vec.y)
    // }

    // pub fn at_vec(&self, vec: Vec2) -> &T {
    //     self.at(vec.round().as_u16vec2())
    // }

    // pub fn at_world_xy(&self, x: &i16, y: &i16) -> Option<&T> {
    //     let x = x + self.world_offset.x;
    //     if x < 0 {
    //         return None;
    //     }

    //     let y = self.map_size.y as i16 - (y + self.world_offset.y);
    //     if y < 0 {
    //         return None;
    //     }

    //     self.map.get(x as usize + y as usize * self.map_size_x)
    // }

    // pub fn at_world(&self, vec: I16Vec2) -> Option<&T> {
    //     self.at_world_xy(&vec.x, &vec.y)
    // }

    // pub fn at_world_vec(&self, vec: Vec2) -> Option<&T> {
    //     self.at_world(vec.as_i16vec2())
    // }
}

pub fn calculate_chunk_data(mut commands: Commands, map_size: Res<MapSize>) {
    let world_offset = I16Vec2::new(map_size.0.x as i16 / 2 - 1, map_size.0.y as i16 / 2);
    let map_size = map_size.0 / CHUNK_SIZE;
    let map = Vec::<Chunk>::with_capacity((map_size.x * map_size.y) as usize);

    let chunks = Chunks {
        world_offset,
        map_size,
        map,
    };

    commands.insert_resource(chunks);
}
