use bevy::prelude::*;

use crate::camera::*;
use crate::data::map_position::sync_map_position;
use crate::GameMode;

use self::startup::*;
use self::update::*;

mod startup;
mod update;

pub struct GameModeMainPlugin;

impl Plugin for GameModeMainPlugin {
    fn build(&self, app: &mut App) {
        app //
            .init_resource::<Selection>()
            .add_systems(
                OnEnter(GameMode::MainStartup),
                (
                    (setup_tilemaps, spawn_camera, generate_terrain_data),
                    (fill_tiles_in_base_map, fill_tiles_in_water_map, spawn_pawns),
                    |mut game_mode_state: ResMut<NextState<GameMode>>| {
                        game_mode_state.set(GameMode::MainUpdate)
                    },
                )
                    .chain(),
            )
            .add_systems(
                Update,
                (
                    camera_movement,
                    select_pawns,
                    draw_gizmos_for_selected_pawns,
                    draw_grid_under_cursor,
                    set_destination_for_selection,
                    draw_current_paths,
                )
                    .run_if(in_state(GameMode::MainUpdate)),
            )
            .add_systems(
                PostUpdate,
                (sync_map_position,).run_if(in_state(GameMode::MainUpdate)),
            )
            .add_systems(
                FixedUpdate,
                (
                    calculate_path_for_all_pawns_with_destination,
                    move_pawns_on_path,
                    //
                )
                    .run_if(in_state(GameMode::MainUpdate)),
            )
            .add_systems(
                FixedPostUpdate,
                (
                    sync_map_position,
                    //
                )
                    .run_if(in_state(GameMode::MainUpdate)),
            );
    }
}
